namespace Authorizations.Enums
{
    public enum EAuthHeader
    {
        token,
        expiresIn
    }
}