using System;
using System.Collections.Generic;
using Authorizations.Enums;

namespace Authorizations.Data
{
    [Serializable]
    public class AuthData
    {
        public Dictionary<EAuthHeader, string> accessToken;
        public Dictionary<EAuthHeader, string> refreshToken;

        public AuthData(Dictionary<EAuthHeader, string> accessToken, Dictionary<EAuthHeader, string> refreshToken)
        {
            this.accessToken = accessToken;
            this.refreshToken = refreshToken;
        }
    }
}