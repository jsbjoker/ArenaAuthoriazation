using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Authorizations.Views
{
    public class AuthorizationView : MonoBehaviour
    {
        [SerializeField] private TMP_InputField _loginField;
        [SerializeField] private TMP_InputField _passwordField;
        [SerializeField] private Button _loginButton;

        public event Action<string, string> OnAuthButtonClick;

        private void Awake()
        {
            _loginButton.onClick.AddListener(HandleAuthClick);
        }

        private void HandleAuthClick()
        {
            OnAuthButtonClick?.Invoke(_loginField.text, _passwordField.text);
        }

        private void OnDestroy()
        {
            _loginButton.onClick.RemoveListener(HandleAuthClick);
            OnAuthButtonClick = null;
        }
    }
}