using System.Collections.Generic;
using Authorizations.Data;
using Authorizations.Enums;
using Authorizations.Views;
using Common.MessageSandlers.Controllers;
using Common.TextSerializers.Interfaces;
using UnityEngine;
using UnityEngine.Networking;

namespace Authorizations.Controllers
{
    public class Authorization : MonoBehaviour
    {
        private const string LoginHeader = "login";
        private const string PasswordHeader = "password";
        [SerializeField] private AuthorizationView _view;
        [SerializeField] private UnitaskMessageSandler _sandler;
        [SerializeField] private TextSerializer _textSerializer;

        private void Awake()
        {
            _view.OnAuthButtonClick += TryAuth;
        }

        private async void TryAuth(string login, string password)
        {
            var headers = new Dictionary<string, string>
            {
                [LoginHeader] = login,
                [PasswordHeader] = password
            };
            var result = await _sandler.GetWebRequest(headers);
            if (result.Result != UnityWebRequest.Result.Success) return;
            if (!_textSerializer.Deserialize(result.Message, out AuthData data)) return;
            Debug.LogError(data.accessToken[EAuthHeader.token]);
        }

        private void OnDestroy()
        {
            _view.OnAuthButtonClick -= TryAuth;
        }
    }
}