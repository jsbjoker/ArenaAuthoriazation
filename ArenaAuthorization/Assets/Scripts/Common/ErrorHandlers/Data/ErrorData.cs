using System;

namespace Common.ErrorHandlers.Data
{
    [Serializable]
    public class ErrorData
    {
        public string id;
        public string code;
        public string type;
        public string message;

        public ErrorData(string id, string code, string type, string message)
        {
            this.id = id;
            this.code = code;
            this.type = type;
            this.message = message;
        }
    }
}