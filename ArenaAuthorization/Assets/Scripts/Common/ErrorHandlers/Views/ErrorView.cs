using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Common.ErrorHandlers.Views
{
    public class ErrorView : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _errorMessage;
        [SerializeField] private Button _closeButton;

        private void Awake()
        {
            CloseWindow();
            _closeButton.onClick.AddListener(CloseWindow);
        }

        private void CloseWindow()
        {
            gameObject.SetActive(false);
        }

        public void ShowWindow(string message)
        {
            _errorMessage.text = message;
            gameObject.SetActive(true);
        }

        private void OnDestroy()
        {
            _closeButton.onClick.RemoveListener(CloseWindow);
        }
    }
}