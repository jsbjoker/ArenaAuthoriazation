using Common.ErrorHandlers.Data;
using Common.ErrorHandlers.Views;
using Common.TextSerializers.Interfaces;
using UnityEngine;

namespace Common.ErrorHandlers.Controllers
{
    public class ErrorHandler : MonoBehaviour
    {
        [SerializeField] private ErrorView _errorView;
        [SerializeField] private TextSerializer _textSerializer;

        public void HandleError(string message)
        {
            if (!_textSerializer.Deserialize(message, out ErrorData errorData)) return;
            _errorView.ShowWindow(errorData.message);
        }
        
    }
}