using UnityEngine.Networking;

namespace Common.MessageSandlers.Data
{
    
    public class ResponceData
    {
        public string Message { get; }
        public UnityWebRequest.Result Result { get; }

        public ResponceData(UnityWebRequest.Result result, string message)
        {
            Message = message;
            Result = result;
        }
    }
}