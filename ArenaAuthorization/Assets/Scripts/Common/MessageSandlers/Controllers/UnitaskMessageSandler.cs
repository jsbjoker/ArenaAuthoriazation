using System.Collections.Generic;
using Common.ErrorHandlers.Controllers;
using Common.MessageSandlers.Data;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;

namespace Common.MessageSandlers.Controllers
{
    public class UnitaskMessageSandler : MonoBehaviour
    {
        [SerializeField] private string _url;
        [SerializeField] private ErrorHandler _errorHandler;


        public async UniTask<ResponceData> GetWebRequest(string url, Dictionary<string, string> headers)
        {
            try
            {
                using var request = await UnityWebRequest.Post(url, headers).SendWebRequest();
                return CreateData(request);
            }
            catch (UnityWebRequestException exeption)
            {
                _errorHandler.HandleError(exeption.UnityWebRequest.downloadHandler.text);
                return CreateData(exeption.UnityWebRequest);
            }
        }

        private ResponceData CreateData(UnityWebRequest request) =>
            new ResponceData(request.result, request.downloadHandler.text);
        
        public async UniTask<ResponceData> GetWebRequest(Dictionary<string, string> headers)
        {
            return await GetWebRequest(_url, headers);
        }
    }
}