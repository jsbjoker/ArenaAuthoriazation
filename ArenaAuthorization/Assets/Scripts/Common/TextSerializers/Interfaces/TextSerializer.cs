using UnityEngine;

namespace Common.TextSerializers.Interfaces
{
    public abstract class TextSerializer : MonoBehaviour
    {
        public abstract bool Serialize(object obj, out string serializeData);
        public abstract bool Deserialize<T>(string text, out T config);

    }
}