using Common.TextSerializers.Interfaces;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

namespace Common.TextSerializers.Controllers
{
    public class NewtonsoftTextSerializer : TextSerializer
    {
        private void Awake()
        {
            JsonConvert.DefaultSettings = () =>
            {
                var settings = new JsonSerializerSettings();
                settings.Converters.Add(new StringEnumConverter {CamelCaseText = false});
                return settings;
            };
        }

        public override bool Serialize(object obj, out string serializeData)
        {
            var isSuccess = true;
            serializeData = JsonConvert.SerializeObject(obj, new JsonSerializerSettings()
            {
                Error = delegate(object sender, ErrorEventArgs args) { isSuccess = false; }
            });
            return isSuccess;
        }

        public override bool Deserialize<T>(string text, out T config)
        {
            var isSuccess = true;
            config = JsonConvert.DeserializeObject<T>(text, new JsonSerializerSettings()
            {
                Error = delegate(object sender, ErrorEventArgs args) { isSuccess = false; }
            });
            return isSuccess;
        }
    }
}